package com.mongo.fetchExcel.repository;

import com.mongo.fetchExcel.Entity.ReportContent;
import com.mongo.fetchExcel.Entity.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.query.FluentQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class ReportRepositoryImpl implements ReportRepository{
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public ReportContent save(ReportContent content, String contentCollection) {
        return mongoTemplate.save(content, contentCollection);
    }

    @Override
    public List<ReportContent> findAllReportContents(HashMap<String, Object> params, String collectionName) {
        return findAllReportContents(params, collectionName, 0, null);
    }

    @Override
    public List<ReportContent> findAllReportContents(HashMap<String, Object> params, String collectionName, int limit, String sort) {
        Criteria finalCriteria = Criteria.where("_vsb").is(Visibility.ACTIVE);
        for (Map.Entry<String, Object> e : params.entrySet()) {
            finalCriteria.and("data." + e.getKey()).is(e.getValue());
        }
        Query query = new Query(finalCriteria);
        if (limit != 0) { query.limit(limit); }
        if (sort != null) {
            Sort s = Sort.by(Sort.Direction.DESC, sort);
            query.with(s);
        }
        System.out.println("query result : " + query);

        return mongoTemplate.find(query, ReportContent.class, collectionName);
    }

    @Override
    public <S extends ReportContent> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends ReportContent> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends ReportContent> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ReportContent> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ReportContent> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ReportContent> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ReportContent> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ReportContent> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends ReportContent, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }

    @Override
    public <S extends ReportContent> S save(S entity) {
        return null;
    }

    @Override
    public <S extends ReportContent> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<ReportContent> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public List<ReportContent> findAll() {
        return null;
    }

    @Override
    public List<ReportContent> findAllById(Iterable<String> strings) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(ReportContent entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends String> strings) {

    }

    @Override
    public void deleteAll(Iterable<? extends ReportContent> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<ReportContent> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ReportContent> findAll(Pageable pageable) {
        return null;
    }
}
