package com.mongo.fetchExcel.repository;


import com.mongo.fetchExcel.Entity.ReportContent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface ReportRepository extends MongoRepository<ReportContent, String> {
    ReportContent save(ReportContent content, String contentCollection);

    List<ReportContent> findAllReportContents(HashMap<String, Object> params, String collectionName);

    List<ReportContent> findAllReportContents(HashMap<String, Object> params, String collectionName, int limit, String sort);

}
