package com.mongo.fetchExcel.httpclients;

public enum HttpClientMethod {
    GET, POST, POST_JSON;
}