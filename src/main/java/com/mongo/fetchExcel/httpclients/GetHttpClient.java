package com.mongo.fetchExcel.httpclients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import java.util.HashMap;

@Slf4j
public class GetHttpClient extends IHttpClient {

    @Override
    public <T> T submit(Class<T> clazz) {
        try {
            ResponseEntity<T> response = this.restTemplate.getForEntity(this.uri, clazz);
            logger.debug("GET Request:"+request+" URI:"+this.uri+" Response:{Code:"+response.getStatusCode()+" Body:"+response.getBody()+"}");
            return response.getBody();
        } catch (Exception e) {
            logger.error("GET Request:"+request+" URI:"+this.uri+" Response:"+e.getMessage());
            throw e;
        }
    }

    @Override
    public <T> T exhange(Class<T> clazz) {
        try {
            ResponseEntity<T> response = restTemplate.exchange(this.uri, HttpMethod.GET, this.request, clazz);
            log.info("GET Request:"+request+" URI:"+this.uri+" Response:{Code:"+response.getStatusCode()+"}");
            logger.debug("GET Request:"+request+" URI:"+this.uri+" Response:{Code:"+response.getStatusCode()+" Body:"+response.getBody()+"}");
            return response.getBody();
        } catch (Exception e) {
            logger.error("GET Request:"+request+" URI:"+this.uri+" Response:"+e.getMessage());
            throw e;
        }
    }

    @Override
    protected void apply(HashMap<String, Object> data) throws Exception {
        this.query(data);
    }

}