package com.mongo.fetchExcel.httpclients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.DefaultUriBuilderFactory.EncodingMode;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;
import java.util.Map.Entry;

public abstract class IHttpClient {
    protected Logger logger = LoggerFactory.getLogger(IHttpClient.class);
    protected RestTemplate restTemplate = new RestTemplate();
    protected HttpHeaders headers = new HttpHeaders();
    protected HttpEntity<? extends Object> request;

    protected DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
    protected URI uri;

    public static IHttpClient getHttpClient(HttpClientMethod method) {
        switch(method) {
            case GET :
                return new GetHttpClient();
        }
        return new GetHttpClient();
    }

    public IHttpClient url(String url, Map<String, Object> queryParam) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            // Iterate over the query parameters and add them to the builder
            for (Map.Entry<String, Object> entry : queryParam.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if (value instanceof String) {
                    // If the value is a String, add it as a query parameter
                    builder.queryParam(key, value);
                } else if (value instanceof Iterable) {
                    // If the value is an Iterable (e.g., a List), add each element as a separate query parameter
                    ((Iterable<?>) value).forEach(element -> builder.queryParam(key, element));
                }
            }

            URI uri = builder.build().encode().toUri();
            this.uri = uri;
        }catch (Exception e){
            logger.info("URI builder failed " + e.getMessage());
        }

//        this.uri = this.factory.uriString(url).build();
        return this;
    }

	public IHttpClient headers(HashMap<String, Object> headersMap) {
        for (Entry<String, Object> entry : headersMap.entrySet()) {
            this.headers.add(entry.getKey(), String.valueOf(entry.getValue()));
        }
        request = new HttpEntity<>(headers);
		return this;
	}

    public IHttpClient query(HashMap<String, Object> data) throws Exception {
        this.factory.setEncodingMode(EncodingMode.VALUES_ONLY);

        String tmpUrl = this.uri.toString()+"?";
        List<String> tmpParams = new ArrayList<>();
        for (Entry<String, Object> e : data.entrySet()) {
            Object mapValue = e.getValue();
            if (mapValue instanceof List) {
                List<?> castedMapValue = (List<?>) mapValue;
                for (int i = 0; i < castedMapValue.size(); i++) {
                    tmpParams.add(Objects.toString(castedMapValue.get(i)));
                    tmpUrl += e.getKey()+"={"+e.getKey()+i+"}&";
                }
            } else {
                tmpParams.add(Objects.toString(mapValue));
                tmpUrl += e.getKey()+"={"+e.getKey()+"}&";
            }
        }
        tmpUrl = tmpUrl.substring(0, tmpUrl.length()-1);

        this.uri = this.factory.uriString(tmpUrl).build(tmpParams.toArray());
        return this;
    }

    public IHttpClient data(HashMap<String, Object> data) throws Exception {
        this.apply(data);
        return this;
    }

    protected abstract void apply(HashMap<String, Object> data) throws Exception;

    public abstract <T> T submit(Class<T> clazz) throws Exception;

    public abstract  <T> T exhange(Class<T> clazz) throws Exception;
}