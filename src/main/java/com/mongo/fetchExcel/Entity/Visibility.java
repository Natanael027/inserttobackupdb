package com.mongo.fetchExcel.Entity;

public enum Visibility {
    ACTIVE, PAUSED;
}