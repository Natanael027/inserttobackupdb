package com.mongo.fetchExcel.Entity;

import lombok.Data;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;

@Data
@Document("reports_")
public class ReportContent {
    @Id 
    private String id;
    private HashMap<String, Object> data = new HashMap<>();
    @Setter String createAt;
    @Setter String updateAt;

}