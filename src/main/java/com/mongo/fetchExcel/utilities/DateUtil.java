package com.mongo.fetchExcel.utilities;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
    public static ZoneId ZONE_ID = ZoneId.systemDefault();

    public static boolean isBetween(String startTime, String endTime, long timeMillis) {
        LocalTime startTimeLocal = LocalTime.parse(startTime);
        LocalTime endTimeLocal = LocalTime.parse(endTime);
    
        LocalTime target = Instant.ofEpochMilli(timeMillis).atZone(ZONE_ID).toLocalTime();
        return target.compareTo(startTimeLocal) >= 0 && target.compareTo(endTimeLocal) <= 0;
    }

    public static int dayOfWeek(long timeMillis) {
        return Instant.ofEpochMilli(timeMillis).atZone(ZONE_ID).getDayOfWeek().getValue();
    }

    public static long dateToMillis(String date) {
        LocalDateTime localDateTime = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay();
        return localDateTime.atZone(ZONE_ID).toInstant().toEpochMilli();
    }

	public static String millisToDate(String format, long timeMillis) {
        LocalDateTime localDateTime = Instant.ofEpochMilli(timeMillis).atZone(ZONE_ID).toLocalDateTime();
        return DateTimeFormatter.ofPattern(format).format(localDateTime);
	}

    public  static String getServerTime(int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }

    public static Date calculateToLocalTime(String dateTime, Double timeZone) {
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime));

            int timeToCalculate = (int) (7 - timeZone);
            int minutesToAdd = (timeToCalculate * 60);
            calendar.add(Calendar.MINUTE, minutesToAdd);
            return calendar.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getYesterdayDate() {
        try {
            // Get yesterday's date
            LocalDate yesterday = LocalDate.now().minusDays(1);

            // Format the date as "yyyy-MM-dd"
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String yesterdayString = yesterday.format(formatter);
            return yesterdayString;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}