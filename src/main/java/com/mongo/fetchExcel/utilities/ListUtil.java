package com.mongo.fetchExcel.utilities;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ListUtil {
    private static Logger logger = LoggerFactory.getLogger(ListUtil.class);

    @SafeVarargs
    public static <T> List<T> toList(T... a) {
        return new ArrayList<>(Arrays.asList(a));
    }

    public static boolean hasCommonElement(Collection<?> firstList, Collection<?> secondList) {
        HashSet<Object> tmp = new HashSet<>();
        for (Object o : firstList) {
            tmp.add(o);
        }
        for (Object o : secondList) {
            if (tmp.contains(o)) {
                return true;
            }
        }
        return false;
    }

    public static <K, T> HashMap<K, T> toHashMap(Function<? super T, ? extends K> keyMapper, List<T> list) {
        Map<K, T> map = list.stream().collect(Collectors.toMap(keyMapper, model -> model, (model1, model2) -> model2));
        return new HashMap<>(map);
    }

    public static <K, T> HashMap<K, List<T>> toListHashMap(Function<T, K> keyMapper, List<T> list, Function<T, Boolean> extendedFilter) {
        Map<K, List<T>> map = list.stream()
        .filter((value) -> {
            return extendedFilter.apply(value);
        })
        .collect(Collectors.toMap(keyMapper, value -> new ArrayList<>(Arrays.asList(value)),
            (valueList1, valueList2) -> {
                valueList1.addAll(valueList2);
                return valueList1;
            }));
        return new HashMap<>(map);
    }

    public static HashMap<String, Object> pojoToHashMap(Object obj) {
        return new HashMap<>(new ObjectMapper().convertValue(obj, new TypeReference<Map<String, Object>>() {}));
    }

    public static <T> T hashMapToPojo(HashMap<String, Object> map, Class<T> clazz) {
        return new ObjectMapper().convertValue(map, clazz);
    }

    public interface IValidator<T> {
        boolean filter(T object);
    }

    public static <T, K> List<K> convertList(Collection<T> collection, Class<K> destinationClass, Consumer<K> setters) {
        return collection.stream().map(sourceClass -> {
            try {
                K resultObject = destinationClass.getDeclaredConstructor(sourceClass.getClass()).newInstance(sourceClass);
                if (setters != null) {
                    setters.accept(resultObject);
                }
                return resultObject;
            } catch (Exception e) {
                logger.error("Error constructing instance: ", e);
                return null;
            }
        }).collect(Collectors.toList());
	}

	public static <T> List<T> filter(List<T> items, Predicate<T> predicate) {
        return items.stream().filter(predicate).findAny().stream().collect(Collectors.toList());
    }
    
	public static <T> boolean hasItem(List<T> items, Predicate<T> predicate) {
        return !filter(items, predicate).isEmpty();
	}

    public static boolean matchingStrings(List<String> list, String regex) {
        Pattern p = Pattern.compile(regex);
        for (String string : list)
        {
            if (p.matcher(string).find())
            {
                return true;
            }
        }
        return false;
    }
}