package com.mongo.fetchExcel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class CollectionModel {
    String collectionName;
    String apiKey;
    String filtersValues;
    boolean hasExcel;

    public CollectionModel(String collectionName, String apiKey, String values) {
        this.collectionName = collectionName;
        this.apiKey = apiKey;
        this.filtersValues = values;
        this.hasExcel = false;
    }

    public CollectionModel(String collectionName, String apiKey, String values, boolean hasExcel) {
        this.collectionName = collectionName;
        this.apiKey = apiKey;
        this.filtersValues = values;
        this.hasExcel = hasExcel;
    }
}
