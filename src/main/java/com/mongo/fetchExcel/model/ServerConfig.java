package com.mongo.fetchExcel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerConfig {
    private String ip_address;
    private String username;
    private String password;
    private int remote_port;
    private int channel_timeout;
    private int session_timeout;

}
