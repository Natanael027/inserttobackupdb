package com.mongo.fetchExcel.controller;

import com.jcraft.jsch.*;
import com.mongo.fetchExcel.Entity.ReportContent;
import com.mongo.fetchExcel.Service.ReportService;
import com.mongo.fetchExcel.model.ServerConfig;
import com.mongo.fetchExcel.schedule.Scheduler;
import com.mongo.fetchExcel.utilities.ListUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class ReportRestController {

    @Autowired
    ReportService reportService;

    @GetMapping("/test")
    public Object getApi() throws Exception {
        String url = "http://139.162.40.40:30002/api/allreports";
        List<String> filtersValues = new ArrayList<>();
        filtersValues.add("district::Pekanbaru");
        filtersValues.add("tgl_terima::2024-01-24"); // karna encode ':' -> %3A

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("logic", "AND");
        queryParams.put("filters", filtersValues);

        Object response = reportService.getApi(url, queryParams);
        HashMap<String, Object> data = (HashMap<String, Object>) response;
        for (String i : data.keySet()) {
            System.out.println("key: " + i + " value: " + data.size());
        }

//        ArrayList<HashMap<String, Object>> resultList = (ArrayList<HashMap<String, Object>>) data.get("reports.6128b6cb2f295b0d2abcc0bd");
        ArrayList<ReportContent> resultList = (ArrayList<ReportContent>) data.get("reports.6128b6cb2f295b0d2abcc0bd");
        System.out.println("resultList size: " + resultList.size());

        return resultList;
    }

    @PostMapping("/sendFile")
    public Object sendFileToServer() throws Exception {
        String REMOTE_HOST = "192.168.8.5";
        String USERNAME = "app";
        String PASSWORD = "password";
        int REMOTE_PORT = 1771;
        int SESSION_TIMEOUT = 10000;
        int CHANNEL_TIMEOUT = 5000;

        String projectPath = System.getProperty("user.dir");
        String localFile = projectPath + "/file/excel";
        Session jschSession = null;

        try {
            JSch jsch = new JSch();
            // jsch.setKnownHosts("C:\\Users\\dartmedia\\.ssh\\known_hosts");
            jschSession = jsch.getSession(USERNAME, REMOTE_HOST, REMOTE_PORT);

            // authenticate using private key
            // jsch.addIdentity("/home/mkyong/.ssh/id_rsa");

            // authenticate using password
            jschSession.setPassword(PASSWORD);
            jschSession.connect(SESSION_TIMEOUT);
            Channel sftp = jschSession.openChannel("sftp");
            sftp.connect(CHANNEL_TIMEOUT);

            ChannelSftp channelSftp = (ChannelSftp) sftp;
            System.out.println("localFile" + localFile);
            File localFolder = new File(localFile);

            String remoteFile = "java/excel";
            channelSftp.cd(remoteFile);

            // Send each file in the local folder
            reportService.sendFiles(channelSftp, localFolder);
            channelSftp.exit();

        } catch (JSchException | SftpException e) {
            e.printStackTrace();
        } finally {
            if (jschSession != null) {
                jschSession.disconnect();
            }
        }
        System.out.println("Done");
        return "resultList";
    }

    @GetMapping("/panggilScheduler")
    public String testing() throws Exception {
        String url = "http://139.162.40.40:30002/api/allreports";
        List<String> filtersValues = new ArrayList<>();
        filtersValues.add("district::Pekanbaru");
        filtersValues.add("tgl_terima::2024-01-24"); // karna encode ':' -> %3A

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("logic", "AND");
        queryParams.put("filters", filtersValues);

        Object response = reportService.getApi(url, queryParams);
        HashMap<String, Object> data = (HashMap<String, Object>) response;
        for (String i : data.keySet()) {
            System.out.println("key: " + i + " value: " + data.size());
        }

        ArrayList<HashMap<String, Object>> resultList = (ArrayList<HashMap<String, Object>>) data.get("reports.6128b6cb2f295b0d2abcc0bd");
        for (HashMap<String, Object> res : resultList) {
            ReportContent result = ListUtil.hashMapToPojo(res, ReportContent.class);
            String excelLink = "";
            excelLink = reportService.findValue(result.getData(), "f");

            if (!excelLink.isEmpty()) {
                String destinationFolder = "./file/excel";
                try {
                    reportService.downloadImage(excelLink, destinationFolder);
                    System.out.println("uri -> " + excelLink + " Excel downloaded successfully!");
                } catch (IOException e) {
                    System.out.println( "Excel downloaded failed -> "+e.getMessage());
                    e.printStackTrace();
                }
            }else {
                System.out.println("Excel link not found, id : " + result.getId());
            }
//            reportService.saveToDB(result, "report_excelReport");
        }

        return "";
    }

}
