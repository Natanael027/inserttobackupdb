package com.mongo.fetchExcel.Service;

import com.jcraft.jsch.*;
import com.mongo.fetchExcel.Entity.ReportContent;
import com.mongo.fetchExcel.httpclients.HttpClientMethod;
import com.mongo.fetchExcel.httpclients.IHttpClient;
import com.mongo.fetchExcel.model.ServerConfig;
import com.mongo.fetchExcel.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Service
public class ReportService {
    @Autowired
    ReportRepository reportRepository;

    public Object getApi(String url, Map<String, Object> queryParams) throws Exception {
        HashMap<String, Object> header = new HashMap<>();
        header.put("API-Key", "abcdefghijklmnopqrstuvwxyz2021");

        Object response = IHttpClient.getHttpClient(HttpClientMethod.GET)
                .url(url, queryParams)
                .headers(header)
                .exhange(Object.class);

        return response;
    }

    public Object getApiWithHeader(String url, Map<String, Object> queryParams, String headerKey) throws Exception {
        HashMap<String, Object> header = new HashMap<>();
        header.put("API-Key", headerKey);

        Object response = IHttpClient.getHttpClient(HttpClientMethod.GET)
                .url(url, queryParams)
                .headers(header)
                .exhange(Object.class);

        return response;
    }

    public String findValue(Map<String, Object> map, String targetKey) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (key.equals(targetKey)) {
                // Key found, return the value
                return (String) value; // Assuming the value is always a String
            }

            if (value instanceof Map) {
                // Recursively search in nested map
                String result = findValue((Map<String, Object>) value, targetKey);
                if (result != null) {
                    return result; // Found in the nested map
                }
            }
        }

        // Key not found in this map
        return null;
    }

    public void downloadImage(String imageUrl, String destinationFolder) throws IOException {
        URL url = new URL(imageUrl);

        // Open a connection to the image URL
        try (InputStream in = url.openStream()) {
            // Extract the file name from the URL
            String fileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);

            // Create the destination folder if it doesn't exist
            Path destinationPath = Path.of(destinationFolder, fileName);
            Files.createDirectories(destinationPath.getParent());

            // Save the image to the destination folder
            Files.copy(in, destinationPath, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public void sendFiles(ChannelSftp channelSftp, File localFolder) throws SftpException {
        for (File file : localFolder.listFiles()) {
            if (file.isFile()) {
                // Send the file to the remote folder
                channelSftp.put(file.getAbsolutePath(), file.getName());
            } else if (file.isDirectory()) {
                // Recursively send files in subdirectories
                channelSftp.mkdir(file.getName());
                channelSftp.cd(file.getName());
                sendFiles(channelSftp, file);
                channelSftp.cd(".."); // Move back to parent directory after sending subdirectory
            }
        }
    }

    public void sendFileToServer(ServerConfig config, String localPath, String remotePath) throws Exception {

//		String localPath = "./file/excel/result-231206134422065.xlsx";
        Session jschSession = null;

        try {
            JSch jsch = new JSch();
//            jsch.setKnownHosts("C:\\Users\\dartmedia\\.ssh\\known_hosts");
            jschSession = jsch.getSession(config.getUsername(), config.getIp_address(), config.getRemote_port());

            // authenticate using private key
            // jsch.addIdentity("/home/mkyong/.ssh/id_rsa");

            // authenticate using password
            jschSession.setPassword(config.getPassword());
            jschSession.connect(config.getSession_timeout());
            Channel sftp = jschSession.openChannel("sftp");
            sftp.connect(config.getChannel_timeout());

            ChannelSftp channelSftp = (ChannelSftp) sftp;
            File localFolder = new File(localPath);

            channelSftp.cd(remotePath);

            // Send each file in the local folder
            sendFiles(channelSftp, localFolder);

            channelSftp.exit();

        } catch (JSchException | SftpException e) {
            e.printStackTrace();
        } finally {
            if (jschSession != null) {
                jschSession.disconnect();
            }
        }
        System.out.println("Done");
    }

    public String saveToDB(ReportContent reportContent, String collectionName){
        ReportContent savedContent = reportRepository.save(reportContent, collectionName);
        return savedContent.getId();
    }
}
