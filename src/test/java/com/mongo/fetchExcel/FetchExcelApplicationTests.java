package com.mongo.fetchExcel;

import com.mongo.fetchExcel.Entity.ReportContent;
import com.mongo.fetchExcel.Service.ReportService;
import com.mongo.fetchExcel.model.CollectionModel;
import com.mongo.fetchExcel.model.ServerConfig;
import com.mongo.fetchExcel.utilities.ListUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
class FetchExcelApplicationTests {
	@Autowired
	ReportService reportService;

	@Test
	void contextLoads() throws Exception {
		System.out.println("Scheduler started " + new Date());
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		String url = "http://139.162.40.40:30002/api/allreports";
		List<String> filtersValues = new ArrayList<>();
		filtersValues.add("tgl_terima::"+date); // karna encode ':' -> %3A

		Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("logic", "AND");
		queryParams.put("filters", filtersValues);

		Object response = reportService.getApi(url, queryParams);
		HashMap<String, Object> data = (HashMap<String, Object>) response;
		ArrayList<HashMap<String, Object>> resultList = (ArrayList<HashMap<String, Object>>) data.get("reports.6128b6cb2f295b0d2abcc0bd");
		System.out.println("Data fetched total data : " + resultList.size());
		for (HashMap<String, Object> res : resultList) {
			ReportContent result = ListUtil.hashMapToPojo(res, ReportContent.class);
			String excelLink = "";
			excelLink = reportService.findValue(result.getData(), "f");

			if (!excelLink.isEmpty()) {
				String destinationFolder = "./file/excel";
				try {
					reportService.downloadImage(excelLink, destinationFolder);
					System.out.println("uri -> " + excelLink + " Excel downloaded successfully!");
				} catch (IOException e) {
					System.out.println( "Excel downloaded failed -> "+e.getMessage());
//                    e.printStackTrace();
				}
			}else {
				System.out.println("Excel link not found, id : " + result.getId());
			}
			reportService.saveToDB(result, "report_excelReport");
		}
	}

	@Test
	void testing() throws Exception {
		String url = "http://139.162.40.40:30002/api/allreports";
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		List<String> filtersValues = new ArrayList<>();
		filtersValues.add("visitdate::"+date); // karna encode ':' -> %3A

		Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("logic", "AND");
		queryParams.put("filters", filtersValues);

		String apiKey = "eifuqwneinvowmvoppokqefqiefjn";
		Object response = reportService.getApiWithHeader(url, queryParams, apiKey);
		HashMap<String, Object> data = (HashMap<String, Object>) response;
		String reportKey = "";
		for (String key : data.keySet()){
			reportKey = key;
		}
		System.out.println(reportKey);

		String collectionName = "report_takingOrder";
		ArrayList<HashMap<String, Object>> resultList = (ArrayList<HashMap<String, Object>>) data.get(reportKey);
		System.out.println("resultList size : " + resultList.size());
		for (HashMap<String, Object> res : resultList) {
			ReportContent result = ListUtil.hashMapToPojo(res, ReportContent.class);
			reportService.saveToDB(result, collectionName);
		}
	}

}
