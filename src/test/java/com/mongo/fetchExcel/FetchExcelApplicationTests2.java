package com.mongo.fetchExcel;

import com.mongo.fetchExcel.Entity.ReportContent;
import com.mongo.fetchExcel.Service.ReportService;
import com.mongo.fetchExcel.schedule.Scheduler;
import com.mongo.fetchExcel.utilities.DateUtil;
import com.mongo.fetchExcel.utilities.ListUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@SpringBootTest
class FetchExcelApplicationTests2 {
	@Autowired
	ReportService reportService;

	private String date = DateUtil.getYesterdayDate();
	private final List<String> collectionNames = Arrays.asList(
			"report_takingOrder", "report_visitationAssignment", "report_visitationWOAssignment", "report_visitationAssignment_1", "masterdata_location",
			"masterdata_targetTahunan", "masterdata_target_bulanan", "masterdata_data_pengiriman", "masterdata_sparepart", "masterdata_jenisMesin",
			"masterdata_info_visit", "masterdata_masalah_kualitas", "masterdata_masalah_aplikasi", "masterdata_masalah_lainnya", "masterdata_productLemindo",
			"masterdata_aplikasi_produk", "masterdata_invoice", "masterdata_delivery", "masterdata_customer", "masterdata_kompetitor",
			"masterdata_email_recipient", "masterdata_realDataDelivery"
	);

	private final List<String> params = Arrays.asList(
			dateParam("tgl_terima"), dateParam("visitdate"), dateParam("actualvisitdate"), dateParam("visitdate"), dateParam("_submitDate"),
			dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"),
			dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"),
			dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"), dateParam("_submitDate"),
			dateParam("_submitDate"), dateParam("_submitDate")
	);

	private final List<String> apiKeys = Arrays.asList(
			"abcdefghijklmnopqrstuvwxyz2021", "eifuqwneinvowmvoppokqefqiefjn", "8447691FD5B3875393976195E71C1", "9A1AFF5E9DD2A6A4481AE3C5F7947", "adjsnvasmdkam12343jdas",
			"ankd81928ejnjaksd912hn3jd", "njkznciunandjksnin123891273rbfjs", "akdjfnskfnioqdnqodpcqimpqoq", "9FCCBA92749FB576F4259A6761D33", "1388122B166A998D3C5A84F254216",
			"8A855C81D818D61C2FA423647A589", "28B458B74F4C9826837F34CE4F847", "FB94E8DB8265913554CE4DB9656B4", "6237C76FB45C5A23D5B9C232F6ED4", "47DB9F8D3F8A9F284A146EBA3E893",
			"2D635B2A2AF752BEF19AE58E16CB4", "A474B4136E37D481E95285F94BE1B", "9F39BFF2946A81439C5BC81216F93", "5C4D64AF4DAAA8AFFFCCA21AE36E8", "183699A8E4D277EF85D511E4C3E39",
			"DA8D1CE564931C8BDAAE31ABF49DE", "D7C6413864FECF2B449C136B7A932"
	);

	protected Logger logger = LoggerFactory.getLogger(Scheduler.class);

	private String dateParam(String param){
		return param+"::"+date;
	}

	@Test
	public void fetchExcel() throws Exception {
//        String url = "http://139.162.40.40:30002/api/allreports?logic=AND&filters=district%3A%3APekanbaru&filters=tgl_terima%3A%3A2024-01-24";
		logger.info("Scheduler started, " + new Date());
		insertToDb();
		logger.info("Scheduler finished, " + new Date());
	}

	void insertToDb() throws Exception {
		for (int i = 0; i<collectionNames.size() ; i++) {
			logger.info("Data fetch started from : " + collectionNames.get(i));
			try {
				String url = "http://139.162.40.40:30002/api/allreports";
				Map<String, Object> queryParams = new HashMap<>();
				queryParams.put("filters", params.get(i));

				Object response = reportService.getApiWithHeader(url, queryParams, apiKeys.get(i));
				HashMap<String, Object> data = (HashMap<String, Object>) response;
				String reportKey = "";
				for (String key : data.keySet()) {
					reportKey = key;
				}

				int totalExcelDownloaded = 0;
				List<String> reportSize = new ArrayList<>();
				ArrayList<HashMap<String, Object>> resultList = (ArrayList<HashMap<String, Object>>) data.get(reportKey);
				logger.info("Data fetched successfully, total data fetched : " + resultList.size());

				for (int m = 0; m < resultList.size(); m++) {
					ReportContent result = ListUtil.hashMapToPojo(resultList.get(m), ReportContent.class);
					if (i == 0) {
						String excelLink = "";
						excelLink = reportService.findValue(result.getData(), "f");

						if (!excelLink.isEmpty()) {
							String destinationFolder = "./file/excel";
							try {
								reportService.downloadImage(excelLink, destinationFolder);
								//logger.info("uri -> " + excelLink + " Excel downloaded successfully!");
								totalExcelDownloaded += 1;
							} catch (IOException e) {
								logger.error("Excel downloaded failed -> " + e.getMessage());
								e.printStackTrace();
							}
						} else {
							logger.info("Excel link not found, id : " + result.getId());
						}
					}

//                    String savedReport = reportService.saveToDB(result, collectionNames.get(i));
//                    reportSize.add(savedReport);
				}
				String msg = "Data inserted successfully into " + collectionNames.get(i) + ", total data inserted : " + reportSize.size();
				msg =  (collectionNames.get(i).equalsIgnoreCase("report_takingOrder")) ? msg + ", total excel downloaded : " + totalExcelDownloaded : msg;
				logger.info(msg);
			} catch (Exception e) {
				logger.error("InsertToDb failed, cause : " + e.getMessage());
			}
		}

	}

	@Test
	public void testDate(){
		LocalDate yesterday = LocalDate.now().minusDays(1);
		LocalDate today = LocalDate.now().minusDays(1);

		System.out.println(date);
		System.out.println("yesterday : " + yesterday);
		System.out.println("today : " + today);
	}



}
